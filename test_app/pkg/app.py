from aiohttp import web
import os

from settings import Settings
from views import VenicleView
import mongo
import background_tasks
from utils import init_logger


def setup_routes(app):
    app.router.add_view("/", VenicleView, name="index")


def make_app():
    app = web.Application()

    log_path = os.path.join(
        os.path.abspath(os.getenv("DIR_LOG", "./logs")),
        "test_app")
    app["logger"] = init_logger(log_path, log_level=os.environ.get("LOGLEVEL"))
    app["settings"] = Settings()
    setup_routes(app)
    mongo.setup(app)
    background_tasks.setup(app)
    return app


def main():
    app = make_app()
    web.run_app(app, port=7777)


if __name__ == "__main__":
    main()
