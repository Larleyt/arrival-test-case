import logging
from pydantic import BaseModel, ValidationError, validator


class Vehicle(BaseModel):
    component: str
    country: str = "USA"  # default value
    description: str
    model: str

    @validator("country")
    def not_empty(cls, v):
        if v == "":
            return "USA"
        return v

    @staticmethod
    async def count_pages(db):
        return await db.vehicles.count_documents({})

    @staticmethod
    def get_page(db, page_num):
        cursor = db.vehicles.find().skip(page_num - 1).limit(1).to_list(1)
        return cursor

    @staticmethod
    async def create(db, data):
        # this validates the data before we insert it into the db
        try:
            vehicle = Vehicle(**data)
            await db.vehicles.update_one(
                {"count": {"$lt": 50}},
                {
                    "$push": {"vehicles": data},
                    "$inc": {"count": 1}
                },
                upsert=True
            )
        except ValidationError as e:
            await logging.info(e)


