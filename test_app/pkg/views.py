from aiohttp import web
from bson import json_util
import json
from collections import OrderedDict
from urllib.parse import urljoin

from models import Vehicle


class VenicleView(web.View):
    async def get(self):
        page_num = int(self.request.query.get("page_num", 1))

        total_pages = await Vehicle.count_pages(self.request.app["mongo"])

        # 1 page = 1 bucket = 50 docs,
        # bucketing allows to lower amount of requests
        data = await Vehicle.get_page(self.request.app["mongo"], page_num)

        get_page_url = lambda qs: urljoin(
            str(self.request.url),
            self.request.app.router["index"].url_for().with_query(qs).human_repr()
        )

        page = OrderedDict({
            "prev": get_page_url({"page_num": str(page_num - 1)}) if page_num > 1 else "",
            "next": get_page_url({"page_num": str(page_num + 1)}) if page_num < total_pages else "",
            "total_pages": total_pages,
            "data": json.loads(json_util.dumps(data))
        })
        return web.json_response(page)
