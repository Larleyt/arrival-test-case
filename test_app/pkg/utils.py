import os
import logging
from datetime import datetime


def init_logger(log_path, log_level=None):
    if log_level is None:
        log_level = logging.DEBUG

    if not os.path.exists(log_path):
        os.makedirs(log_path)

    log_filename = os.path.join(
            log_path,
            "{}.log".format(datetime.now().strftime("%Y%m%d_%H%M%S")))

    log_formatter = logging.Formatter(
        "%(asctime)s %(levelname)s [%(filename)s:%(lineno)d:%(funcName)s] [%(threadName)s]: %(message)s")

    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    # configure file handler
    fh = logging.FileHandler(log_filename)
    fh.setFormatter(log_formatter)

    # configure stream handler
    ch = logging.StreamHandler()
    ch.setFormatter(log_formatter)

    logger = logging.getLogger()

    # set the logging level
    logger.setLevel(log_level)

    # if not len(logger.handlers):
    logger.addHandler(fh)
    logger.addHandler(ch)
    return logger
