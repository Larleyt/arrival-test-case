import aiohttp
import asyncio
import json
import logging
import os
from json import JSONDecodeError

from models import Vehicle


async def fetch_client(url, session):
    async with session.get(url) as response:
        return await response.read()


async def get_vehicle_data(app):
    request_url = f"ws://{os.getenv('VENICLE_EMULATOR_CONTAINER_NAME')}:8080"
    session = aiohttp.ClientSession()

    try:
        async with session.ws_connect(request_url) as ws:
            async for msg in ws:
                if msg.type == aiohttp.WSMsgType.TEXT:
                    try:
                        await Vehicle.create(app["mongo"], json.loads(msg.data))
                    except JSONDecodeError as e:
                        logging.warning(f"{e}: {msg.data}")
                        continue
                elif msg.type == aiohttp.WSMsgType.CLOSED:
                    break
                elif msg.type == aiohttp.WSMsgType.ERROR:
                    break
    except asyncio.CancelledError:
        pass


async def start_background_tasks(app):
    app["listener"] = asyncio.create_task(get_vehicle_data(app))


async def cleanup_background_tasks(app):
    app["listener"].cancel()
    await app["listener"]


def setup(app):
    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)
