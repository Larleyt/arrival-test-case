from pydantic import BaseSettings
import os


class Settings(BaseSettings):
    db_name = os.getenv("MONGO_DB_NAME", "arrival_test_db")
    user = os.getenv("MONGO_INITDB_ROOT_USERNAME", "")
    password = os.getenv("MONGO_INITDB_ROOT_PASSWORD", "")
    mongo_uri = f"mongodb://{user}:{password}@mongo"
