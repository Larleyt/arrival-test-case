##  How to run the project

Create a docker network `my-net`:

`$ docker network create my-net`
 
The network name is pre-defined in `test_app/docker-compose.yml` and would be created automatically at `docker-compose up`
but first you need to connect `vehicleEmulator` to it.

(**Note**: if you start `vehicleEmulator` after `test_app`, the latter won't be able to connect to the websocket in this simple setup.)

```
$ cd path/to/vehicleEmulator
$ docker build . -t v_emul
$ docker run --name v_emul -it --network=test_app_my-net v_emul 
```

Configure `.env` files inside `test_app` folder as you please 
(e.g. you've built `vehicleEmulator` image with a name other than `v_emul` or need to change MongoDB auth credentials)
and then build and run `test_app`:

```
$ cd path/to/test_app
$ docker build . -t api:latest && docker-compose -f docker-compose.yml up
```

Now `test_app` has started filling up `test_db` inside `mongo` container with the data from a websocket 
while ignoring invalid jsons and changing empty `country` field to be "USA".
Visit the `127.0.0.1:7777/?page_num=N` to check its contents in a paginated manner.
